/*
	Much like other data types, objects can also be grouped into an array. 
	Here, all array methods will also be implemented.
*/

let users = [
	{
		username: "mike9900",
		email: "michaeljames@gmail.com",
		password: "mikecutie1999",
		isAdmin: false
	},
	{
		username: "justin89",
		email: "justinetlake@gmail.com",
		password: "iamnsync00",
		isAdmin: true
	}
];

//How can we access items in an array of objects?
console.log(users[0]);
console.log(users[1]);
//Just like how we access items in an array, we first get the name of the array and then access the index.

//Log the email property of the second object in the array?
console.log(users[1].email);

//Log the username property of the first object in the array?
console.log(users[0]["username"]);

//Can Array Methods also work on an array of objects?

//What method can we use to add new objects at the end of the array?

users.push({
	username: "abbeyarcher",
	email: "abbeyarrows@gmail.com",
	password: "bowandabbey",
	isAdmin: false
})

console.log(users);

let newUser = {
	username: "hanna1993",
	email: "hannafight@gmail.com",
	password: "fighting1993",
	isAdmin: false
}

users.push(newUser);

console.log(users);

class User {
	constructor(username,email,password){
		this.username = username;
		this.email = email;
		this.password = password;
	}
}

let user1 = new User("kateduchess","nottherealone@gmail.com","imnotroyalty");
users.push(user1);

console.log(users);

/*Mini-Activity*/

users.push({
	username: "sweetguy",
	email: "sweetguy@gmail.com",
	password: "niceandsweet",
	isAdmin: false
})

let user2 = new User("niceguy","niceguy@gmail.com","imnotnice");
users.push(user2);

console.log(users);

users.push(new User("newuser1","newuser@gmail.com","newuser8"));

console.log(users);

//Register function

	//Tip: When creating a function which will receive data as an argument, always console log the parameters first.

	function register(username,email,password){
		/*console.log(username);
		console.log(email);
		console.log(password);*/

		/*Mini-Activity*/

		if (username.length < 8 || email.length < 8 || password.length < 8 ){
			alert("Details provided too short. username, email and password must be more than 8 characters")
		} else {

			//register function should be able to push a new user object in our array:
			//console.log(users); console.log() everything!	

			users.push({
				username: username,
				email: email,
				password: password,
				isAdmin: false
			})
		}
	}

	register("mynameisjeff","jeff@gmail.com","namesnotjeff");
	//register("short","short","short");
	console.log(users);

//Login function
	
	//find() - check our array if there is a user which matches the email and password provided/input in the function.

	//find() is like forEach and map wherein it will iterate/loop over each item in the array and then return a condition. If the condition returned restults to true, the find() will return the item that is currently iterated or looped over.

	function login(emailInput,pwInput){
		//console.log(emailInput);
		//console.log(pwInput);

		//Anonymous function in the find() method receives the current item being looped over or iterated.
		let foundUser = users.find((user)=>{

			//In find(), forEach(), map(), we loop over every item in the array.
				/*
					In the first iteration:

					user = first item in the array

					michaeljames@gmail.com === jeff@gmail.com
					//false
					mikecutie1999 === namesnotjeff
					//false

					user.email === emailInput && user.password ===pwInput//false

					return false

					//If the anonymous function in find() returns false, we will continue to the next item.

					In the second iteration:

					user = second item in the array

					justintlake@gmail.com === jeff@gmail.com
					//false
					iamnsync00 === namesnotjeff
					//false

					user.email === emailInput && user.password ===pwInput//false

					return false

					//If the anonymous function in find() returns false, we will continue to the next item.

					//find() will continue until either the anonymous function returns true or until we finished looping over the array.

					In the last iteration:

					user = last item in the array

					jeff@gmail.com === jeff@gmail.com
					//true
					namesnotjeff === namesnotjeff
					//true

					user.email === emailInput && user.password ===pwInput//true

					return true

					//If item is found, find() will return the item and we can save it in a variable.

					//If no item returns true, find() will return undefined.
				*/

			//console.log(user.email);
			//console.log(user.password);
			//We're going to check each user's email and password if it matches our emailInput and pwInput.
			return user.email === emailInput && user.password ===pwInput;
		})

		// console.log(foundUser);

		if(foundUser !== undefined){
			console.log("Thank you for logging in.")
		} else {
			console.log("Invalid Credentials. No User Found.")
		}
	}

	login("jeff@gmail.com","namesnotjeff");