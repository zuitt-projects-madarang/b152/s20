let courses = [];

const postCourse = (id,name,description,price,isActive) => {
	courses.push({
		id: id,
		name: name,
		description: description,
		price: price,
		isActive: isActive
	})
	alert(`You have created ${name}. The price is ${price}.`)
}

postCourse("101","Math","Basic Mathematics",1000,true);
postCourse("102","English","Basic English",15000,true);
postCourse("103","Japanese","Basic Japanese",2000,true);
console.log(courses);


const getSingleCourse = (idInput) => {
	let foundId = courses.find((course)=>{
		return course.id === idInput;
	})
	//console.log(foundId);
	if(foundId !== undefined){
		console.log(foundId);
	} else {
		console.log("Invalid ID. No Course Found.");
	}
}

getSingleCourse("101");


const deleteCourse = () => {
	courses.pop();
}

deleteCourse();

console.log(courses);