/*Mini- Activity*/

let assets = [
	{
		id: "001",
		name: "Nintendo Switch",
		description: "Gaming Console",
		stock: 50,
		isAvailable: true,
		dateAdded: "Jan. 1, 2022"
	},
	{
		id: "002",
		name: "Nintendo Wii",
		description: "Gaming Console",
		stock: 0,
		isAvailable: false,
		dateAdded: "Sept. 5, 2015"
	}
];

console.log(assets);

//JSON - Javascript Object Notation

	//JSON is a string formatted as a JS Object.
	//JSON is popularly used to pass data from one application to another.
	//Strings are easier to transfer from one application to another.

	//JSON is a string.
	//JS object is an object.
	//JSON Keys are surrounded by double quotes.
	//JSON extra comma at the end will become an error.

	let jsonSample = `{
		"sampleKey1": "valueA",
		"sampleKey2": "valueB",
		"sampleKey3": 1,
		"sampleKey4": true
	}`

	console.log(typeof jsonSample);

 	let jsonConvert = JSON.parse(jsonSample);
 	console.log(typeof jsonConvert);
 	console.log(jsonConvert);

 	//JSON is not only used in JS but also in other programming languages.
 	//This is why it is specified as Javascript Object Notation.
 	//There are also JSON that are saved in a file with extension called .json

 	//There is also a way to turn JSON into JS Object and turn JS Objects into JSON.

 	//Converting JS Objects into JSON

 		//This will allow us to convert/turn JS Objects into JSON
 		//This is how JSON is manipulated even when received by other applications.

 		let batches = [
 			{
 				batch: `Batch 152`
 			},
 			{
 				batch: `Batch 156`
 			}
 		];

 		//To turn JS Objects into JSON, we use the method JSON.stringify().
 		//This method will return JSON format out of the object that we pass as an argument.

 		let batchesJSON = JSON.stringify(batches);
 		console.log(batchesJSON);

 		let data = {
 			name: "Katniss",
 			age: 20,
 			address: {
 				city: "Kansas City",
 				state: "Kansas"
 			}
 		}

 		let dataJSON = JSON.stringify(data);
 		console.log(dataJSON);

 		//JSON.stringify is commonly used when trying to pass data from one application to another via HTTP requests. HTTP requests are requests for data between the client(browser/page/app) and a server.

 		/*Mini-Activity*/

 		let data2 = {
 			username: "saitamaOPM",
 			password: "onepuuuuunch",
 			isAdmin: true
 		}

 		let data3 = {
 			username: "lightYagami",
 			password: "notKiraDefinitely",
 			isAdmin: false
 		}

 		let data4 = {
 			username: "Llawliett",
 			password: "yagamiiskira07",
 			isAdmin: false
 		}

 		let data2JSON = JSON.stringify(data2);
 		let data3JSON = JSON.stringify(data3);
 		let data4JSON = JSON.stringify(data4);
 		let jsonArray = JSON.stringify(assets);
 		
 		console.log(data2JSON);
 		console.log(data3JSON);
 		console.log(data4JSON);
		console.log(jsonArray);


		//Can we still use JS array methods on JSON arrays?
		//No, because JSON is a string.
		/*jsonArray.pop();
		console.log(jsonArray);*/

		//JSON.parse()

			let items = `[
				{
					"id": "shop-1",
					"name": "Oreos",
					"stock": 5,
					"price": 50
				},
				{
					"id": "shop-2",
					"name": "Doritos",
					"stock": 10,
					"price": 150
				}
			]`

			let itemsJSArr = JSON.parse(items);
			console.log(itemsJSArr);

			//JSON.parse() turns JSON into JS Objects.

			itemsJSArr.pop();
			console.log(itemsJSArr);

			//JSON.stringify() the itemsJSArr and re-assign to items:
			items = JSON.stringify(itemsJSArr);
			console.log(items);

			/*
				Communicating Data with JSON

				Server - page/browser (Client)

				-Client send data in JSON format
				-Server receives data in JSON format
					-To be able to manipulate/process the data sent
						-parse the JSON back into JS object
					-stringify the data back to JSON
					-send it back to the Client (page/browser)
				-Client receives data in JSON format
			*/

		/*Mini-Activity*/

		let courses = `[
			{
				"name": "Math 101",
				"description": "learn the basics of Math",
				"price": 2500
			},
			{
				"name": "Science 101",
				"description": "learn the basics of Science",
				"price": 2500
			}
		]`

		//console.log(courses);

		let coursesJSArr = JSON.parse(courses);
		//console.log(coursesJSarr);
		coursesJSArr.pop();
		console.log(coursesJSArr);
		courses = JSON.stringify(coursesJSArr);
		console.log("Courses JSON array after deleteing:")
		console.log(courses);

		let coursesJSArr2 = JSON.parse(courses);
		//console.log(coursesJSArr2);
		coursesJSArr2.push({
			name: "English 101",
			description: "learn the basics of English",
			price: 2500
		})
		console.log(coursesJSArr2);
		courses = JSON.stringify(coursesJSArr2);
		console.log("Courses JSON array after adding:")
		console.log(courses);